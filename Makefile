# Makefile for crmath LaTeX package

mainsource := $(shell grep -l '^[^%]*\\documentclass' *.dtx)
package = $(mainsource:.dtx=)
packfiles = ${package}.dtx ${package}.ins ${package}.pdf Makefile
tempfiles = *.aux *.log *.glo *.ind *.idx *.out *.svn *.toc *.ilg *.gls *.hd *.synctex.gz

generated = ${package}.pdf ${package}.sty ${package}.tar.gz ${package}.dvi ${package}.ps

.PHONY: all doc package new clean clean_all

all: package doc clean message
new: clean_all all

doc: ${package}.pdf

package: ${package}.sty

${package}.pdf: ${package}.dtx ${package}.sty
	pdflatex ${package}.dtx
	pdflatex ${package}.dtx
	pdflatex ${package}.dtx
	makeindex -s gind.ist -o ${package}.ind ${package}.idx
	makeindex -s gglo.ist -o ${package}.gls ${package}.glo
	pdflatex ${package}.dtx
	pdflatex -synctex=1 ${package}.dtx

${package}.sty: ${package}.dtx ${package}.ins
	-@rm -f ${package}.sty
	latex ${package}.ins

clean:
	-@rm -f ${tempfiles}

clean_all:
	-@rm -f ${tempfiles} ${generated} *~

tar.gz: package doc ${package}.tar.gz

${package}.tar.gz:
	tar -czf $@ ${packfiles}

message:
	@echo
	@echo
	@echo "*************************************************************"
	@echo "*                          DONE"
	@echo "*************************************************************"
	@echo "* You might want to move the newly created $(package).sty and"
	@echo "* $(package).pdf to your texmf tree, e.g:"
	@echo "*    ~/texmf/tex/latex/$(package)/$(package).sty"
	@echo "* and"
	@echo "*    ~/texmf/doc/$(package)/$(package).pdf"
	@echo "* respectively."
	@echo "*************************************************************"
	@echo
