:: Windows batch script for crmath LaTeX package

set pck=crmath

:: The package
del /q %pck%.sty
latex %pck%.ins

:: The documentation
pdflatex %pck%.dtx
pdflatex %pck%.dtx
pdflatex %pck%.dtx
pdflatex %pck%.dtx
makeindex -s gind.ist -o %pck%.ind %pck%.idx
makeindex -s gglo.ist -o %pck%.gls %pck%.glo
pdflatex %pck%.dtx
pdflatex -synctex=1 %pck%.dtx

:: Clean up
del /q %pck%.aux %pck%.log %pck%.glo %pck%.gls %pck%.ind %pck%.idx %pck%.out %pck%.svn %pck%.toc %pck%.ilg %pck%.hd
